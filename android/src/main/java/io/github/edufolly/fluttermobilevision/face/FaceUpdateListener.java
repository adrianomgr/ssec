package io.github.edufolly.fluttermobilevision.face;

import android.support.annotation.UiThread;

import com.google.android.gms.vision.face.Face;

public interface FaceUpdateListener {

    @UiThread
    void onFaceDetected(Face item);

}

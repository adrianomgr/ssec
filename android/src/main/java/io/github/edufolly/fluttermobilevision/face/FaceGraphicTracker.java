package io.github.edufolly.fluttermobilevision.face;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;

import io.github.edufolly.fluttermobilevision.ui.GraphicOverlay;


class FaceGraphicTracker extends Tracker<Face> {

    private GraphicOverlay<FaceGraphic> overlay;
    private FaceGraphic graphic;

    private FaceUpdateListener faceUpdateListener;

    public FaceGraphicTracker(GraphicOverlay<FaceGraphic> overlay, FaceGraphic graphic, FaceUpdateListener faceUpdateListener) {
        this.overlay = overlay;
        this.graphic = graphic;
        this.faceUpdateListener = faceUpdateListener;
    }

    @Override
    public void onNewItem(int id, Face face) {
        graphic.setId(id);
    }

    @Override
    public void onUpdate(Detector.Detections<Face> detectionResults, Face item) {
        overlay.add(graphic);
        graphic.updateItem(item);
        if (item.getIsLeftEyeOpenProbability() < 0.10 || item.getIsRightEyeOpenProbability() < 0.10 || item.getIsSmilingProbability() > 0.90) {
            faceUpdateListener.onFaceDetected(item);
        } 
    }

    @Override
    public void onMissing(Detector.Detections<Face> detectionResults) {
        overlay.remove(graphic);
    }

    @Override
    public void onDone() {
        overlay.remove(graphic);
    }
}

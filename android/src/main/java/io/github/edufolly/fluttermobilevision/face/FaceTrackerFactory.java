package io.github.edufolly.fluttermobilevision.face;

import android.util.Log;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;

import io.github.edufolly.fluttermobilevision.ui.GraphicOverlay;

public class FaceTrackerFactory implements MultiProcessor.Factory<Face> {
    private GraphicOverlay<FaceGraphic> graphicOverlay;
    private boolean showText;
    private String adriano;
    private FaceUpdateListener faceUpdateListener;

    public FaceTrackerFactory(GraphicOverlay<FaceGraphic> graphicOverlay, boolean showText, String adriano, FaceUpdateListener faceUpdateListener) {
        this.graphicOverlay = graphicOverlay;
        this.showText = showText;
        this.adriano = adriano;
        this.faceUpdateListener = faceUpdateListener;
    }

    @Override
    public Tracker<Face> create(Face face) {
        FaceGraphic graphic = new FaceGraphic(graphicOverlay, showText, adriano);
        try {
            return new FaceGraphicTracker(graphicOverlay, graphic, faceUpdateListener);
        } catch (Exception ex) {
            Log.d("FaceTrackerFactory", ex.getMessage(), ex);
        }
        return null;
    }
}

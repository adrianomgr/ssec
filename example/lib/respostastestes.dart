import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './testes.dart';
import './perguntas.dart';

class RespostaTestes extends StatefulWidget {

  int _resposta;
  int _idquestion;
  String resposta;

  RespostaTestes(this._resposta,this._idquestion);
  

  @override
  State createState() => new RespostaTestesState();
}

class RespostaTestesState extends State<RespostaTestes> with SingleTickerProviderStateMixin {
  
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
  bool lastquestion;
  int lengthquestions = 3;
  


  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    _iconAnimationController = new AnimationController(duration: new Duration(seconds: 2), vsync: this);
    _iconAnimation = new CurvedAnimation(parent: _iconAnimationController, curve: Curves.elasticOut);
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
    if(widget._resposta == 5 && widget._idquestion == 0){
      widget.resposta = "Muito bem!!";
      widget._idquestion++;
    }
    else if(widget._resposta == 6 && widget._idquestion == 1){
      widget.resposta = "Muito bem!!";
      widget._idquestion++;
    }
    else if(widget._resposta == 7 && widget._idquestion == 2){
      widget.resposta = "Muito bem!! Vamos para as Perguntas!";
      widget._idquestion++;
    }
    else{
      widget._resposta = 4;
      widget.resposta = "Erro da resposta, vamos tentar novamente!";
    }
    if(widget._idquestion==lengthquestions)
      lastquestion = true;
    Future.delayed(Duration(seconds: 5), () => Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => lastquestion == true ? new Perguntas(true, 0, []) : new Testes(widget._idquestion))));
  }
  @override
  void dispose() {
    _iconAnimationController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.black54,
      child: new InkWell(
        onTap: () => Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => lastquestion == true ? new Perguntas(true, 0, []) : new Testes(widget._idquestion))),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle
              ),
              child: new Transform.rotate(
                angle: _iconAnimation.value * 2 * 3.1415,
                child: new Icon(widget._resposta == 4 ? Icons.clear : Icons.done, size: _iconAnimation.value * 80.0,),
              ),
            ),
            new Padding(
              padding: new EdgeInsets.only(bottom: 20.0),
            ),
            new Text(widget.resposta, style: new TextStyle(color: Colors.white, fontSize: 30.0),)
          ],
        ),
      ),
    );
  }

}
import 'package:flutter/services.dart';

import 'dart:async';

import 'package:flutter/material.dart';

import './perguntas.dart';
import './resultado.dart';

class Resposta extends StatefulWidget {
  int _resposta;
  int _idquestion;
  bool _isGestos;
  String resposta;
  List<int> resultado = List();

  Resposta(this._resposta, this._idquestion, this._isGestos, this.resultado);

  @override
  State createState() => new RespostaState();
}

class RespostaState extends State<Resposta>
    with SingleTickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
  bool lastquestion;
  int lengthquestions = 12;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    _iconAnimationController = new AnimationController(
        duration: new Duration(seconds: 2), vsync: this);
    _iconAnimation = new CurvedAnimation(
        parent: _iconAnimationController, curve: Curves.elasticOut);
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
    if (widget._resposta == 1) {
      widget.resposta = "NÃO";
      widget.resultado.add(widget._resposta);
      widget.resultado[widget._idquestion] = widget._resposta;
      widget._idquestion++;
    } else if (widget._resposta == 2) {
      widget.resposta = "SIM";
      widget.resultado.add(widget._resposta);
      widget.resultado[widget._idquestion] = widget._resposta;
      widget._idquestion++;
    } else if (widget._resposta == 3) {
      widget.resposta = "Talvez";
      widget.resultado.add(widget._resposta);
      widget.resultado[widget._idquestion] = widget._resposta;
      widget._idquestion++;
    } else if (widget._resposta == 4) {
      widget.resposta = "Resposta não identificada";
    }
    if (widget._idquestion == lengthquestions) lastquestion = true;
    Future.delayed(
        Duration(seconds: 5),
        () => Navigator.of(context).pushReplacement(new MaterialPageRoute(
            builder: (BuildContext context) => lastquestion == true
                ? new Resultado(widget.resultado)
                : new Perguntas(
                    widget._isGestos, widget._idquestion, widget.resultado))));
  }

  @override
  void dispose() {
    _iconAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.black54,
      child: new InkWell(
        onTap: () => Navigator.of(context).pushReplacement(
            new MaterialPageRoute(
                builder: (BuildContext context) => lastquestion == true
                    ? new Resultado(widget.resultado)
                    : new Perguntas(widget._isGestos, widget._idquestion,
                        widget.resultado))),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                  color: Colors.white, shape: BoxShape.circle),
              child: new Transform.rotate(
                angle: _iconAnimation.value * 2 * 3.1415,
                child: new Icon(
                  widget._resposta == 4 ? Icons.clear : Icons.done,
                  size: _iconAnimation.value * 80.0,
                ),
              ),
            ),
            new Padding(
              padding: new EdgeInsets.only(bottom: 20.0),
            ),
            new Text(
              widget.resposta,
              style: new TextStyle(color: Colors.white, fontSize: 30.0),
            )
          ],
        ),
      ),
    );
  }
}

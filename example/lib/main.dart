import 'dart:async';

import 'package:flutter/services.dart';

import 'package:flutter/material.dart';

import './instrucoes.dart';

void main() => runApp(
      new MaterialApp(debugShowCheckedModeBanner: false, home: new MyApp()),
    );

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
   @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    Future.delayed(
        Duration(seconds: 5),
        () => Navigator.of(context).pushReplacement(
            new MaterialPageRoute(
                builder: (BuildContext context) => new Instrucoes(true))));
  }
  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.greenAccent,
      child: new InkWell(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Row(
              // This is our main page
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  "IMD",
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 40.0,
                      fontWeight: FontWeight.bold),
                ),
                new Text(
                  " Instituto Metrópole Digital",
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 25.0,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
            new Padding(
              padding: EdgeInsets.all(20),
            ),
            new Text(
              "SSEC",
              style: new TextStyle(
                  color: Colors.white,
                  fontSize: 50.0,
                  fontWeight: FontWeight.bold),
            ),
            new Text(
              "Sistema Especialista para Educação e Comunicação",
              style: new TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontWeight: FontWeight.w400),
            ),
            Container(
              padding: EdgeInsets.all(30),
              child: Row(
                // This is our main page
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () => Navigator.of(context).pushReplacement(
                        new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new Instrucoes(true))),
                    color: Colors.white,
                    child: Text("Gestos"),
                    textColor: Colors.black,
                  ),
                  Padding(
                    padding: EdgeInsets.all(30),
                  ),
                  RaisedButton(
                    onPressed: () => Navigator.of(context).pushReplacement(
                        new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new Instrucoes(false))),
                    color: Colors.white,
                    child: Text("Toque"),
                    textColor: Colors.black,
                  )
                ],
              ),
            ),
            new Text(
              "Espere 5 Segundos para iniciar por gestos",
              style: new TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}

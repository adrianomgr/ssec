import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import './respostastestes.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';

class Testes extends StatefulWidget {
  int _numquestion;
  Testes(this._numquestion);

  @override
  State createState() => new TestesState();
}

class Question {
  final int id;
  final String question;

  Question(this.id, this.question);
}

class TestesState extends State<Testes> {
  int _cameraFace = FlutterMobileVision.CAMERA_FRONT;
  bool _autoFocusFace = true;
  bool _torchFace = false;
  bool _multipleFace = false;
  bool _showTextFace = true;
  int _resposta = 0;
  List<Question> _questions = List();

  @override
  void initState() {
    super.initState();
SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    _questions.add(Question(0, "Pisque o Olho Esquerdo para NÃO!"));
    _questions.add(Question(1, "Pisque o Olho Direito para SIM!"));
    _questions.add(Question(2, "Sorria para TALVEZ!"));
    _face();
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Material(
                      color: Colors.white,
                      child: new Padding(
                        padding: new EdgeInsets.symmetric(vertical: 20.0),
                        child: new Center(
                          child: new Text(
                            _questions.elementAt(widget._numquestion).question,
                            style: new TextStyle(fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              new Expanded(
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Material(
                        color: Colors.redAccent,
                        child: new InkWell(
                          onTap: () => Navigator.of(context).pushReplacement(
                              new MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      new RespostaTestes(
                                          5, widget._numquestion))),
                          child: new Center(
                              child: new Container(
                            decoration: new BoxDecoration(
                                border: new Border.all(
                                    color: Colors.white, width: 5.0)),
                            padding: new EdgeInsets.all(20.0),
                            child: new Text("Não",
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 40.0,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic)),
                          )),
                        ),
                      ),
                    ),
                    new Expanded(
                      child: new Material(
                        color: Colors.greenAccent,
                        child: new InkWell(
                          onTap: () => Navigator.of(context).pushReplacement(
                              new MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      new RespostaTestes(
                                          6, widget._numquestion))),
                          child: new Center(
                            child: new Container(
                                decoration: new BoxDecoration(
                                    border: new Border.all(
                                        color: Colors.white, width: 5.0)),
                                padding: new EdgeInsets.all(20.0),
                                child: new Text("Sim",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 40.0,
                                        fontWeight: FontWeight.bold,
                                        fontStyle: FontStyle.italic))),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              new Material(
                color: Colors.lightBlueAccent,
                child: new InkWell(
                  onTap: () => Navigator.of(context).pushReplacement(
                      new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new RespostaTestes(7, widget._numquestion))),
                  child: new Padding(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: new Center(
                      child: new Container(
                          decoration: new BoxDecoration(
                              border: new Border.all(
                                  color: Colors.white, width: 5.0)),
                          padding: new EdgeInsets.all(10.0),
                          child: new Text("Talvez",
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.italic))),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<Null> _face() async {
    List<Face> faces = [];
    try {
      faces = await FlutterMobileVision.face(
        flash: _torchFace,
        autoFocus: _autoFocusFace,
        multiple: _multipleFace,
        showText: _showTextFace,
        camera: _cameraFace,
        fps: 15.0,
        adriano: _questions.elementAt(widget._numquestion).question,
      );
    } on Exception {
      faces.add(new Face(-1));
      _resposta = 4;
    }
    setState(() {
      if (faces.last.leftEyeOpenProbability <
              faces.last.rightEyeOpenProbability &&
          faces.last.leftEyeOpenProbability <
              (1 - faces.last.smilingProbability)) {
        _resposta = 5;
      } else if (faces.last.leftEyeOpenProbability >
              faces.last.rightEyeOpenProbability &&
          faces.last.rightEyeOpenProbability <
              (1 - faces.last.smilingProbability)) {
        _resposta = 6;
      } else if (1 - faces.last.smilingProbability <
              faces.last.leftEyeOpenProbability &&
          faces.last.rightEyeOpenProbability >
              (1 - faces.last.smilingProbability)) {
        _resposta = 7;
      }
      print(_resposta);
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) =>
              new RespostaTestes(_resposta, widget._numquestion)));
    });
  }
}

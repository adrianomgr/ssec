import 'package:flutter/material.dart';
import './main.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart';

class Professor {
  List<int> respostas;
  String nome;
  String email;
  String site;
  String img;
  int matchs;
  String sala;
  Professor(this.respostas, this.nome, this.email, this.site, this.img,
      this.sala, this.matchs);
}

class Resultado extends StatefulWidget {
  final List<int> resultado;

  Resultado(this.resultado);
  @override
  State createState() => new ResultadoState();
}

class ResultadoState extends State<Resultado>
    with SingleTickerProviderStateMixin {
  List<Professor> professores = List();
  List<Widget> listresultado = new List<Widget>();

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    professores.add(Professor(
        [2, 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 1],
        'Prof. Carlos Eduardo',
        'kaduardo@imd.ufrn.br',
        'http://lattes.cnpq.br/0125036186628507',
        'assets/kadu.jpg',
        'Sala B311',
        0));
    professores.add(Professor(
        [1, 2, 2, 1, 1, 1, 2, 1, 1, 1, 1, 2],
        'Prof. Charles Andryê',
        'charles@imd.ufrn.br',
        'http://lattes.cnpq.br/2381782879955327',
        'assets/charles.jpg',
        'Sala B305',
        0));
    professores.add(Professor(
        [1, 1, 2, 1, 2, 2, 1, 1, 1, 1, 2, 1],
        'Prof. Eiji Adachi',
        'eijiadachi@imd.ufrn.br',
        'http://lattes.cnpq.br/8833409749475821',
        'assets/eiji.jpg',
        'Sala B313',
        0));
    professores.add(Professor(
        [2, 1, 2, 1, 1, 1, 1, 1, 2, 2, 1, 1],
        'Prof. Frederico Lopes',
        'fred@imd.ufrn.br',
        'http://lattes.cnpq.br/9177823996895375',
        'assets/fred.jpg',
        'Sala B312',
        0));
    professores.add(Professor(
        [2, 1, 1, 1, 2, 1, 1, 2, 2, 1, 1, 1],
        'Prof. Gustavo Girão',
        'girao@imd.ufrn.br',
        'http://lattes.cnpq.br/9491033611706611',
        'assets/girao.jpg',
        'Sala B310',
        0));
    professores.add(Professor(
        [1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 2, 1],
        'Prof. João Carlos',
        'jcxavier@imd.ufrn.br',
        'http://lattes.cnpq.br/5088238300241110',
        'assets/joao.jpg',
        'Sala B309',
        0));
    professores.add(Professor(
        [1, 1, 2, 1, 2, 1, 2, 1, 1, 1, 2, 1],
        'Prof. Leonardo César',
        'leobezerra@imd.ufrn.br',
        'http://lattes.cnpq.br/0664132257054306',
        'assets/leobez.jpg',
        'Sala B314',
        0));
    professores.add(Professor(
        [1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 2],
        'Prof. Leonardo Cunha',
        'leonardo@dimap.ufrn.br',
        'http://lattes.cnpq.br/9064196799520278',
        'assets/leocunha.jpg',
        'Sala B324',
        0));
    professores.add(Professor(
        [2, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2],
        'Prof. Rummenigge',
        'rudson@ect.ufrn.br',
        'http://lattes.cnpq.br/1868960602254610',
        'assets/rumm.jpg',
        'Sala B315',
        0));
    professores.add(Professor(
        [1, 1, 2, 1, 2, 1, 2, 1, 1, 1, 2, 1],
        'Prof. Sérgio Queiroz',
        'sergiomedeiros@ect.ufrn.br',
        'http://lattes.cnpq.br/0310395336626784',
        'assets/sergio.jpg',
        'Sala B306',
        0));
    professores.add(Professor(
        [2, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1],
        'Prof. Silvio Costa',
        'silviocs@imd.ufrn.br',
        'http://lattes.cnpq.br/3526197867529103',
        'assets/silvio.jpg',
        'Sala B304',
        0));
    professores.add(Professor(
        [2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1],
        'Prof. Uirá Kulesza',
        'uira@dimap.ufrn.br',
        'http://lattes.cnpq.br/0189095897739979',
        'assets/uira.jpg',
        'Sala B306',
        0));
    for (Professor p in professores) {
      int i = 0;
      for (int res in p.respostas) {
        if (res == widget.resultado[i]) {
          p.matchs++;
          p.matchs++;
        } else if (widget.resultado[i] == 3) {
          p.matchs++;
        }
        i++;
      }
    }
    //ordem de matchs
    professores.sort((y, x) => x.matchs.compareTo(y.matchs));
    //criando o widget dos professores
    for (Professor f in professores) {
      f.matchs = (f.matchs * 100 / 24).toInt();
      listresultado.add(new ResultadoProfessor(f));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.blueAccent,
      child: new SingleChildScrollView(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              "Resultado ",
              style: new TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 50.0),
            ),
            new Column(children: listresultado),
          ],
        ),
      ),
    );
  }
}

class ResultadoProfessor extends StatelessWidget {
  final Professor data;
  ResultadoProfessor(this.data);

  _launchURL() async {
    var url = data.site;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Não carregou $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
      child: Row(
        children: <Widget>[
          Container(
              margin: const EdgeInsets.only(right: 16.0),
              child: CircleAvatar(backgroundImage: new AssetImage(data.img)),
              width: 130.0,
              height: 130.0),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                data.nome,
                style: new TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 30.0),
              ),
              Container(
                margin: const EdgeInsets.only(top: 5.0),
                child: Text(
                  data.email,
                  style: new TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 20.0),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 5.0),
                child: Text(
                  data.sala,
                  style: new TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 20.0),
                ),
              ),
              RaisedButton(
                color: Colors.white,
                child: Text("Lattes"),
                textColor: Colors.blueAccent,
                onPressed: _launchURL,
              ),
            ],
          )),
          Container(
              margin: const EdgeInsets.only(left: 16.0),
              child: CircleAvatar(
                child: Text(
                  (data.matchs).toString() + '%',
                  style: new TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 40.0),
                ),
              ),
              width: 100.0,
              height: 100.0),
        ],
      ),
    );
  }
}

import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import './main.dart';
import './perguntas.dart';
import './testes.dart';


class Instrucoes extends StatefulWidget {

  final bool is_gestos;

  Instrucoes(this.is_gestos);

  @override
  State createState() => new InstrucoesState();
}

class InstrucoesState extends State<Instrucoes> {

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    Future.delayed(
        Duration(seconds: 15),
        () => Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => widget.is_gestos == true ? new Testes(0) : new Perguntas(widget.is_gestos,0,[]))));
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.blueAccent,
      child: new InkWell(
      onTap: () => Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => widget.is_gestos == true ? new Testes(0) : new Perguntas(widget.is_gestos,0,[]))),
      child: new SingleChildScrollView(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
                new Text("Instruções ", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 50.0),),
                new Padding(padding: EdgeInsets.symmetric(vertical: 10.0),),
                new Text("O SSEC é um jogo de perguntas e respostas que visa auxiliar os estudantes a escolher seus orientadores.", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 20.0),textAlign: TextAlign.center),
                new Padding(padding: EdgeInsets.symmetric(vertical: 12.0),),
                new Text("O jogo possui três alternativas:", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 20.0),textAlign: TextAlign.center),
                new Text("SIM, NÃO e TALVEZ", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20.0),textAlign: TextAlign.center),
                new Padding(padding: EdgeInsets.symmetric(vertical: 12.0),),
                new Text('No modo gestual, piscar os olhos (direito e esquerdo) equivale respectivamente a "SIM" ou "NÃO" e sorrir a "TALVEZ".', style: new TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 20.0),textAlign: TextAlign.center),
                new Padding(padding: EdgeInsets.symmetric(vertical: 12.0),),
                new Text('Responda de acordo com sua intenção de pesquisa.', style: new TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 20.0),textAlign: TextAlign.center),
                new Padding(padding: EdgeInsets.symmetric(vertical: 12.0),),
                new Text('15 Segundos', style: new TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 10.0),textAlign: TextAlign.center),
                
          ],
        ),
      ),
      ),
    );
  }
}


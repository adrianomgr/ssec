import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import './respostas.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';

class Perguntas extends StatefulWidget {
  bool _isGestos;
  int _numquestion;
  List<int> resultado = List();
  Perguntas(this._isGestos, this._numquestion, this.resultado);

  @override
  State createState() => new PerguntasState();
}

class Question {
  final int id;
  final String question;

  Question(this.id, this.question);
}

class PerguntasState extends State<Perguntas> {
  int _cameraFace = FlutterMobileVision.CAMERA_FRONT;
  bool _autoFocusFace = true;
  bool _torchFace = false;
  bool _multipleFace = false;
  bool _showTextFace = true;
  int _resposta = 0;
  List<Question> _questions = List();

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    _questions.add(Question(0, "Tem afinidade com Internet das coisas?"));
    _questions.add(Question(1, "Pensa em desenvolver Jogos Digitais?"));
    _questions.add(Question(2, "Seu pensamento utiliza Machine Learning?"));
    _questions.add(Question(3, "Envolve conhecimentos de Redes?"));
    _questions.add(Question(4, "Tem aprofundamento em Compiladores?"));
    _questions.add(Question(5, "Utiliza conhecimentos de Segurança da Informação?"));
    _questions.add(Question(6, "Necessita de Mineração de Dados?"));
    _questions.add(Question(7, "Precisa de conhecimentos de Arquitetura de Computadores?"));
    _questions.add(Question(8, "Utiliza-se de Sistemas Embarcados?"));
    _questions.add(Question(9, "Há aprofundamento em Computação em nuvens?"));
    _questions.add(Question(10, "Utiliza-se análise desenvolvimento de software?"));
    _questions.add(Question(11, "Envolve Realidade Virtual?"));

    if (widget._isGestos) _face();

  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Material(
                      color: Colors.white,
                      child: new Padding(
                        padding: new EdgeInsets.symmetric(vertical: 20.0),
                        child: new Center(
                          child: new Text(
                            _questions.elementAt(widget._numquestion).question,
                            style: new TextStyle(fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              new Expanded(
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Material(
                        color: Colors.redAccent,
                        child: new InkWell(
                          onTap: () => Navigator.of(context).pushReplacement(
                              new MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      new Resposta(1, widget._numquestion,
                                          false, widget.resultado))),
                          child: new Center(
                              child: new Container(
                            decoration: new BoxDecoration(
                                border: new Border.all(
                                    color: Colors.white, width: 5.0)),
                            padding: new EdgeInsets.all(20.0),
                            child: new Text("Não",
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 40.0,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic)),
                          )),
                        ),
                      ),
                    ),
                    new Expanded(
                      child: new Material(
                        color: Colors.greenAccent,
                        child: new InkWell(
                          onTap: () => Navigator.of(context).pushReplacement(
                              new MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      new Resposta(2, widget._numquestion,
                                          false, widget.resultado))),
                          child: new Center(
                            child: new Container(
                                decoration: new BoxDecoration(
                                    border: new Border.all(
                                        color: Colors.white, width: 5.0)),
                                padding: new EdgeInsets.all(20.0),
                                child: new Text("Sim",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 40.0,
                                        fontWeight: FontWeight.bold,
                                        fontStyle: FontStyle.italic))),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              new Material(
                color: Colors.lightBlueAccent,
                child: new InkWell(
                  onTap: () => Navigator.of(context).pushReplacement(
                      new MaterialPageRoute(
                          builder: (BuildContext context) => new Resposta(3,
                              widget._numquestion, false, widget.resultado))),
                  child: new Padding(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: new Center(
                      child: new Container(
                          decoration: new BoxDecoration(
                              border: new Border.all(
                                  color: Colors.white, width: 5.0)),
                          padding: new EdgeInsets.all(10.0),
                          child: new Text("Talvez",
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.italic))),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<Null> _face() async {
    List<Face> faces = [];
    try {
      faces = await FlutterMobileVision.face(
        flash: _torchFace,
        autoFocus: _autoFocusFace,
        multiple: _multipleFace,
        showText: _showTextFace,
        camera: _cameraFace,
        fps: 15.0,
        adriano: _questions.elementAt(widget._numquestion).question,
      );
    } on Exception {
      faces.add(new Face(-1));
      _resposta = 4;
    }
    setState(() {
      if (faces.last.leftEyeOpenProbability <
              faces.last.rightEyeOpenProbability &&
          faces.last.leftEyeOpenProbability <
              (1 - faces.last.smilingProbability)) {
        _resposta = 1;
      } else if (faces.last.leftEyeOpenProbability >
              faces.last.rightEyeOpenProbability &&
          faces.last.rightEyeOpenProbability <
              (1 - faces.last.smilingProbability)) {
        _resposta = 2;
      } else if (1 - faces.last.smilingProbability <
              faces.last.leftEyeOpenProbability &&
          faces.last.rightEyeOpenProbability >
              (1 - faces.last.smilingProbability)) {
        _resposta = 3;
      }
      print(_resposta);
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new Resposta(
              _resposta, widget._numquestion, true, widget.resultado)));
    });
  }
}
